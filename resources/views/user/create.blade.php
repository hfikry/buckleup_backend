<div class="panel-group col-md-6 col-sm-12" id="accordion" style="padding-left: 0">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <i class="fa fa-plus"></i>
                    Add a New User                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">

                <form action="/user" method="post">

                    {{ csrf_field() }}

                    {!! \Nvd\Crud\Form::input('role_id','text')->show() !!}

                    {!! \Nvd\Crud\Form::input('name','text')->show() !!}

                    {!! \Nvd\Crud\Form::input('email','text')->show() !!}

                    {!! \Nvd\Crud\Form::input('fb_id','text')->show() !!}

                    {!! \Nvd\Crud\Form::input('fb_token','text')->show() !!}

                    {!! \Nvd\Crud\Form::input('bio','text')->show() !!}

                    {!! \Nvd\Crud\Form::input('nationality','text')->show() !!}

                    {!! \Nvd\Crud\Form::input('phone_number','text')->show() !!}

                    {!! \Nvd\Crud\Form::input('bdate','date')->show() !!}

                    {!! \Nvd\Crud\Form::input('rego','text')->show() !!}

                    {!! \Nvd\Crud\Form::input('license','text')->show() !!}

                    {!! \Nvd\Crud\Form::textarea( 'paypal_email' )->show() !!}

                    {!! \Nvd\Crud\Form::input('gmail','text')->show() !!}

                    {!! \Nvd\Crud\Form::input('image_url','text')->show() !!}

                    {!! \Nvd\Crud\Form::input('avg_reviews','text')->show() !!}

                    {!! \Nvd\Crud\Form::textarea( 'password' )->show() !!}

                    {!! \Nvd\Crud\Form::input('device_type','text')->show() !!}

                    {!! \Nvd\Crud\Form::textarea( 'push_token' )->show() !!}

                    {!! \Nvd\Crud\Form::textarea( 'remember_token' )->show() !!}

                    {!! \Nvd\Crud\Form::input('active','text')->show() !!}

                    <button type="submit" class="btn btn-primary">Create</button>

                </form>

            </div>
        </div>
    </div>
</div>