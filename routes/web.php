<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('loaderio-16942feb577c3139f5c62d72da2e0fb5.txt', function () {
    // return view('welcome');
    $headers = ['Content-type'=>'text/plain'];
    return response()->download(public_path()."/loaderio-16942feb577c3139f5c62d72da2e0fb5.txt", "loaderio-16942feb577c3139f5c62d72da2e0fb5.txt", $headers);

});
Route::resource('user','UserController');
