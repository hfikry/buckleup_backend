<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class UserReview extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_receiver', 'user_writer', 'text','stars','seats','trip_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];


}
