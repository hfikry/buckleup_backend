<?php
/* @var $url */
/* @var $record */
?>
<td class="actions-cell">
<form class="form-inline" action="/<?php echo e($url); ?>/<?php echo e($record->id); ?>" method="POST">
    <a href="/<?php echo e($url); ?>/<?php echo e($record->id); ?>"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;

    <a href="/<?php echo e($url); ?>/<?php echo e($record->id); ?>/edit"><i class="fa fa-pencil-square-o"></i></a>

    <?php echo e(csrf_field()); ?>

    <?php echo e(method_field('DELETE')); ?>

    <button style="outline: none;background: transparent;border: none;"
            onclick="return confirm('Are You Sure?')"
            type="submit" class="fa fa-trash text-danger"></button>
</form>
</td>