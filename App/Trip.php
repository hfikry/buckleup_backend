<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Trip extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_poster','paypal_email','total_seats', 'user_joiner','status', 'trip_from','trip_to','seats','price_per_seat','date_time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];


}
