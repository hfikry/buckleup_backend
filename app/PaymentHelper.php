<?php
namespace App;

 class PaymentHelper {


public function getAccessToken($code){
  $curl = curl_init();
  // $code = '';
  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.sandbox.paypal.com/v1/identity/openidconnect/tokenservice?grant_type=authorization_code&code=".$code,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //  CURLOPT_USERPWD => 'ARJc4HgfMcVTH4Z8fv3XW7K2lEzV3KFGe_d6qlZ_oRl17ljShJAiPqz17VuO3XsnhvUNxnhjiBiSue5v:EHzLkyHcS_Zo6Z6HarSlO1iFGJ_yIuQsiBaihrUHXtaU0knG2mdf39z1SAuix3Di3QAC07qGRvMF30IZ',
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "authorization: Basic QVJKYzRIZ2ZNY1ZUSDRaOGZ2M1hXN0sybEV6VjNLRkdlX2Q2cWxaX29SbDE3bGpTaEpBaVBxejE3VnVPM1hzbmh2VU54bmhqaUJpU3VlNXY6RUh6TGt5SGNTX1pvNlo2SGFyU2xPMWlGR0pfeUl1UXNpQmFpaHJVSFh0YVUwa25HMm1kZjM5ejFTQXVpeDNEaTNRQUMwN3FHUnZNRjMwSVo=",
      "cache-control: no-cache",
      "postman-token: ff081827-61f8-d49d-fbd0-c8092009167d"
    ),
  ));

  $response = curl_exec($curl);
  $jsonResponse = json_decode($response);

  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
    return $jsonResponse->access_token;
  }
}
public function getEmail($access_token){


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.sandbox.paypal.com/v1/identity/openidconnect/userinfo/?schema=openid",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "authorization_code=A21AAFKUgqarKNNkidrQPrGNAePO-jQHmkH6q7S2uX8eUiPy2n0y-ZlUHZXMpSB2fbpByFKtc7oUJf5jjH8aKg_QK_CB5AqyA&grant_type=authorization_code3&=",
  CURLOPT_HTTPHEADER => array(
    "authorization: Bearer ".$access_token,
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 7c37b519-c147-f4dd-d65b-9fb2429e99f4"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
$jsonResponse = json_decode($response);
$email = $jsonResponse->email;
if ($err) {
  echo "cURL Error #:" . $err;
} else {
  return $jsonResponse->email;
}
}

public function deductMoneyUsingMDID($userId, $ammount){
  $user = User::find($userid);
  $mdid = $user->mdid;

  $data = '{
             "intent":"sale",
             "payer":{
                "payment_method":"paypal"
             },
             "transactions":[
                {
                   "amount":{
                      "currency":"AUD",
                      "total":"'.$amount.'"
                   },
                   "description":"Trip payment"
                }
             ],
    "note_to_payer": "Contact us for any questions on your order.",
    "redirect_urls": {
    "return_url": "http://www.paypal.com/return",
    "cancel_url": "http://www.paypal.com/cancel"
    }
  }';
  $ch = curl_init('https://api.sandbox.paypal.com/v1/payments/payment');
  // curl_setopt($ch, CURLOPT_URL, "");
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "Authorization: Bearer A21AAFG2K1btGonS6L42kCkVcJ6ZM_o7opdyWFZwPHDRJxpMZK0wyFjULWLjycO7tdVaLCuEv8dqPXgWljqel1cIxJBIkaLJQ",
    "PayPal-Client-Metadata-Id: ".$mdid)
    // "Content-length: ".strlen($data))
  );

  $result = curl_exec($ch);
  curl_close($ch);
  echo $result;
  // echo $data;


}

}

?>
