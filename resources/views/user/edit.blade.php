@extends('vendor.crud.single-page-templates.common.app')

@section('content')

    <h2>Update User: {{$user->role_id}}</h2>

    <form action="/user/{{$user->id}}" method="post">

        {{ csrf_field() }}

        {{ method_field("PUT") }}

        {!! \Nvd\Crud\Form::input('role_id','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('name','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('email','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('fb_id','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('fb_token','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('bio','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('nationality','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('phone_number','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('bdate','date')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('rego','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('license','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::textarea( 'paypal_email' )->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('gmail','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('image_url','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('avg_reviews','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::textarea( 'password' )->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('device_type','text')->model($user)->show() !!}

        {!! \Nvd\Crud\Form::textarea( 'push_token' )->model($user)->show() !!}

        {!! \Nvd\Crud\Form::textarea( 'remember_token' )->model($user)->show() !!}

        {!! \Nvd\Crud\Form::input('active','text')->model($user)->show() !!}

        <button type="submit" class="btn btn-default">Submit</button>

    </form>

@endsection