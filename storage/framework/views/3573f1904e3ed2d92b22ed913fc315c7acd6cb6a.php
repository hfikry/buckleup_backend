<?php
/* @var $gen \Nvd\Crud\Commands\Crud */
?>
<?='<?php'?>

namespace App\Http\Controllers;

use App\<?php echo e($gen->modelClassName()); ?>;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class <?php echo e($gen->controllerClassName()); ?> extends Controller
{
    public $viewDir = "<?php echo e($gen->viewsDirName()); ?>";

    public function index()
    {
        $records = <?php echo e($gen->modelClassName()); ?>::findRequested();
        return $this->view( "index", ['records' => $records] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->view("create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $this->validate($request, <?php echo e($gen->modelClassName()); ?>::validationRules());

        <?php echo e($gen->modelClassName()); ?>::create($request->all());

        return redirect('/<?php echo e($gen->route()); ?>');
    }

    /**
     * Display the specified resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, <?php echo e($gen->modelClassName()); ?> $<?php echo e($gen->modelVariableName()); ?>)
    {
        return $this->view("show",['<?php echo e($gen->modelVariableName()); ?>' => $<?php echo e($gen->modelVariableName()); ?>]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, <?php echo e($gen->modelClassName()); ?> $<?php echo e($gen->modelVariableName()); ?>)
    {
        return $this->view( "edit", ['<?php echo e($gen->modelVariableName()); ?>' => $<?php echo e($gen->modelVariableName()); ?>] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, <?php echo e($gen->modelClassName()); ?> $<?php echo e($gen->modelVariableName()); ?>)
    {
        if( $request->isXmlHttpRequest() )
        {
            $data = [$request->name  => $request->value];
            $validator = \Validator::make( $data, <?php echo e($gen->modelClassName()); ?>::validationRules( $request->name ) );
            if($validator->fails())
                return response($validator->errors()->first( $request->name),403);
            $<?php echo e($gen->modelVariableName()); ?>->update($data);
            return "Record updated";
        }

        $this->validate($request, <?php echo e($gen->modelClassName()); ?>::validationRules());

        $<?php echo e($gen->modelVariableName()); ?>->update($request->all());

        return redirect('/<?php echo e($gen->route()); ?>');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return  \Illuminate\Http\Response
     */
    public function destroy(Request $request, <?php echo e($gen->modelClassName()); ?> $<?php echo e($gen->modelVariableName()); ?>)
    {
        $<?php echo e($gen->modelVariableName()); ?>->delete();
        return redirect('/<?php echo e($gen->route()); ?>');
    }

    protected function view($view, $data = [])
    {
        return view($this->viewDir.".".$view, $data);
    }

}
