<?php

return [

    'sign_up' => [
        'release_token' => env('SIGN_UP_RELEASE_TOKEN'),
        'validation_rules' => [
            'name' => 'required',
            'email' => 'required|email',
            'fb_id' => 'required'
        ]
    ],

    'login' => [
        'validation_rules' => [
            'email' => 'required|email',
            'password' => 'required'
        ]
    ],

    'forgot_password' => [
        'validation_rules' => [
            'email' => 'required|email'
        ]
    ],
    'add_validation' => [
        'validation_rules' => [

            'token' => 'required',
            'email'=>'required'
        ]
    ],
    'get_profile' => [
        'validation_rules' => [
            'token' => 'required'
        ]
    ],
    'reset_password' => [
        'release_token' => env('PASSWORD_RESET_RELEASE_TOKEN', false),
        'validation_rules' => [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]
    ],
    'require_token' => [

        'validation_rules' => [
          'token' => 'required',
          'id' => 'required'

        ]
    ]
];
