<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use App\PushHelper;
use App\PaymentHelper;

use App\Notification;

use App\UserReview;
use App\FutureReview;


use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\TokenRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Image;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
class UserController extends Controller
{
//test
  public function getMyProfile(TokenRequest $request, JWTAuth $JWTAuth){
  if($user = \JWTAuth::authenticate($request->input('token'))){
    $reviews = DB::select('SELECT `user_reviews`.* ,`users`.`name`,`users`.`image_url`
      FROM `user_reviews` , `users`
      WHERE `users`.`id` = `user_reviews`.`user_writer`
      AND `user_reviews`.`user_receiver`= ?
      ORDER BY `user_reviews`.`created_at` desc',[$user->id]);
      //calculating age
      $from = new \DateTime($user->bdate);
      $to   = new \DateTime('today');
      $user->age = $from->diff($to)->y;
    $user->reviews = $reviews;

    return response()->json(
      $user
    , 200);
  }


  else {
    return response()->json([
        'error' => 'invalid token'
    ], 200);
  }

  }
  public function getUsersProfile(TokenRequest $request, JWTAuth $JWTAuth){
  if($user = \JWTAuth::authenticate($request->input('token'))){
    $profile = User::whereRaw('id = ?',[$request->input('user_id')])->get();
    $reviews =  DB::select('SELECT `user_reviews`.* ,`users`.`name`,`users`.`image_url`
    FROM `user_reviews` , `users`
    WHERE `users`.`id` = `user_reviews`.`user_writer`
    AND `user_reviews`.`user_receiver`= ?
    ORDER BY `user_reviews`.`created_at` desc',[$request->input('user_id')]);
    //calculating age
    $from = new \DateTime($profile[0]->bdate);
    $to   = new \DateTime('today');
    $profile[0]->age = $from->diff($to)->y;
    $profile[0]->reviews = $reviews;

    return response()->json(
      $profile[0]
    , 200);
  }


  else {
    return response()->json([
        'error' => 'invalid token'
    ], 200);
  }

  }
  public function reviewAUser(TokenRequest $request, JWTAuth $JWTAuth)
  {
    if($user = \JWTAuth::authenticate($request->input('token'))){


        $UserReview = new UserReview();
        $request->request->add(['user_writer' => $user->id]);
        $UserReview->fill($request->all());
        $saved = $UserReview->save();

        $avgReview  = DB::select('select avg(stars) as avg from user_reviews where `user_receiver` = ?',[$user->id]);
        if($avgReview[0]->avg == null)
          $avgReview[0]->avg = 0;
          DB::table('users')
          ->where('id',$user->id )  // find your user by their email
          ->limit(1)  // optional - to ensure only one record is updated.
          ->update(array('avg_reviews' => $avgReview[0]->avg));

          $queryResult = DB::table('future_reviews')
                    ->select('future_reviews.*')
                    ->where('future_reviews.user_writer', '=', $user->id)
                    ->where('future_reviews.user_receiver', '=', $request->input('user_receiver'))
                    ->delete();

            // $queryResult[0]->delete();

        $notification = new Notification();
        $notification->user_id = $request->user_receiver;
        $notification->type = 'received_review';
        $notification->text = 'Someone reviewed you';
        $notification->save();


        $userReceiver = User::findOrFail($request->input('user_receiver'));
        $pushHelper = new PushHelper();
        $pushHelper->sendPush($userReceiver->push_token,'You\'ve received a new review!',['code'=>'new_review']);

      return response()->json([
          'status' => 'success',
          'code' => 200

      ], 200);
            try {
    }catch(\Illuminate\Database\QueryException $ex){
        return response()->json([
            'error' => 'Error writing a review'.$ex,
            'code' => 500

        ], 406);
        }
    }else {
        return response()->json([
            'error' => 'invalid token'
        ], 401);
      }
}

public function testPush(TokenRequest $request, JWTAuth $JWTAuth){
  //android
  // $token = 'c3FYfmAK-WA:APA91bGUkZbjz_yojlfSjy7SniycdgHT-MHX2JDds30hT0Imr4DhkGV15hoG-G8I38BmGUKUL8fiTCw8jLzuDQvRXm84-PfptcAtjsRDz2TmFT44t784lhnexmnxB9RtkAz6hyibSnBL';
//iOS
// $token = 'dXmem5h-PcI:APA91bFgMCU5VhnDctkPnIcUEo--cgIAPj2PCpk1Z4bKhvz8iFxveiPVJ89qAkiAECoNlbQXJaSg2Gy7nSd0KVgn6i9fMWvxPowD-6ktXfqOn03-5bLwSMj1Qii5M9MSVklVseYNMv8H';
$token='cC7wWWm22R8:APA91bGVO66y6-4QewCyId_xMKiP-iwblX56lIuERaqkIfBJhhPL2O2dj1MXoBrksdCLDAnhUUqZ4gAG0n8e_S76uCYKTXwpEiSzC_j3fa9EeIe_dch_mjyLB3rd_yG4mk1NLOi_mEFy';
  $push  = new PushHelper();
  $push->sendPush(1,$token,'code','message');


}
public function registerPaypalUser(TokenRequest $request, JWTAuth $JWTAuth){
  if($user = \JWTAuth::authenticate($request->input('token'))){


  $paymentHelper = new PaymentHelper();
  $access_token = $paymentHelper->getAccessToken($request->input('code'));
  $email = $paymentHelper->getEmail($access_token);
  if($email == null)
  {

    // TODO: return 500
    return response()->json([
        'error' => 'invalid authorization code'
    ], 406);
  }
  else {
    // TODO: save to user mode,  return 200
    $user->paypal_email = $email;
    $user->save();
    return response()->json([
        'status' => 'ok'
    ], 200);
  }
}
else {
  return response()->json([
      'error' => 'invalid token'
  ], 401);

}

}

public function getSebToken(TokenRequest $request, JWTAuth $JWTAuth){

  $user = User::find(279);
  $userAuth = \JWTAuth::fromUser($user);
  return response()->json([$userAuth], 200);
}


}
