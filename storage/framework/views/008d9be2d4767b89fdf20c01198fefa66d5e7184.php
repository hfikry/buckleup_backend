<?php $__env->startSection('content'); ?>

	<h2>Users</h2>

	<?php echo $__env->make('user.create', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<table class="table table-striped grid-view-tbl">
	    
	    <thead>
		<tr class="header-row">
			<?php echo \Nvd\Crud\Html::sortableTh('id','user.index','Id'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('role_id','user.index','Role Id'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('name','user.index','Name'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('email','user.index','Email'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('fb_id','user.index','Fb Id'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('fb_token','user.index','Fb Token'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('bio','user.index','Bio'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('nationality','user.index','Nationality'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('phone_number','user.index','Phone Number'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('bdate','user.index','Bdate'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('rego','user.index','Rego'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('license','user.index','License'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('paypal_email','user.index','Paypal Email'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('gmail','user.index','Gmail'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('image_url','user.index','Image Url'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('avg_reviews','user.index','Avg Reviews'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('password','user.index','Password'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('device_type','user.index','Device Type'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('push_token','user.index','Push Token'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('remember_token','user.index','Remember Token'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('created_at','user.index','Created At'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('updated_at','user.index','Updated At'); ?>

			<?php echo \Nvd\Crud\Html::sortableTh('active','user.index','Active'); ?>

			<th></th>
		</tr>
		<tr class="search-row">
			<form class="search-form">
				<td><input type="text" class="form-control" name="id" value="<?php echo e(Request::input("id")); ?>"></td>
				<td><input type="text" class="form-control" name="role_id" value="<?php echo e(Request::input("role_id")); ?>"></td>
				<td><input type="text" class="form-control" name="name" value="<?php echo e(Request::input("name")); ?>"></td>
				<td><input type="text" class="form-control" name="email" value="<?php echo e(Request::input("email")); ?>"></td>
				<td><input type="text" class="form-control" name="fb_id" value="<?php echo e(Request::input("fb_id")); ?>"></td>
				<td><input type="text" class="form-control" name="fb_token" value="<?php echo e(Request::input("fb_token")); ?>"></td>
				<td><input type="text" class="form-control" name="bio" value="<?php echo e(Request::input("bio")); ?>"></td>
				<td><input type="text" class="form-control" name="nationality" value="<?php echo e(Request::input("nationality")); ?>"></td>
				<td><input type="text" class="form-control" name="phone_number" value="<?php echo e(Request::input("phone_number")); ?>"></td>
				<td><input type="date" class="form-control" name="bdate" value="<?php echo e(Request::input("bdate")); ?>"></td>
				<td><input type="text" class="form-control" name="rego" value="<?php echo e(Request::input("rego")); ?>"></td>
				<td><input type="text" class="form-control" name="license" value="<?php echo e(Request::input("license")); ?>"></td>
				<td><input type="text" class="form-control" name="paypal_email" value="<?php echo e(Request::input("paypal_email")); ?>"></td>
				<td><input type="text" class="form-control" name="gmail" value="<?php echo e(Request::input("gmail")); ?>"></td>
				<td><input type="text" class="form-control" name="image_url" value="<?php echo e(Request::input("image_url")); ?>"></td>
				<td><input type="text" class="form-control" name="avg_reviews" value="<?php echo e(Request::input("avg_reviews")); ?>"></td>
				<td><input type="text" class="form-control" name="password" value="<?php echo e(Request::input("password")); ?>"></td>
				<td><input type="text" class="form-control" name="device_type" value="<?php echo e(Request::input("device_type")); ?>"></td>
				<td><input type="text" class="form-control" name="push_token" value="<?php echo e(Request::input("push_token")); ?>"></td>
				<td><input type="text" class="form-control" name="remember_token" value="<?php echo e(Request::input("remember_token")); ?>"></td>
				<td><input type="text" class="form-control" name="created_at" value="<?php echo e(Request::input("created_at")); ?>"></td>
				<td><input type="text" class="form-control" name="updated_at" value="<?php echo e(Request::input("updated_at")); ?>"></td>
				<td><input type="text" class="form-control" name="active" value="<?php echo e(Request::input("active")); ?>"></td>
				<td style="min-width: 6em;"><?php echo $__env->make('vendor.crud.single-page-templates.common.search-btn', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></td>
			</form>
		</tr>
	    </thead>

	    <tbody>
	    	<?php $__empty_1 = true; $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); $__empty_1 = false; ?>
		    	<tr>
					<td>
						<?php echo e($record->id); ?>

						</td>
					<td>
						<span class="editable"
							  data-type="number"
							  data-name="role_id"
							  data-value="<?php echo e($record->role_id); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->role_id); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="name"
							  data-value="<?php echo e($record->name); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->name); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="email"
							  data-name="email"
							  data-value="<?php echo e($record->email); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->email); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="fb_id"
							  data-value="<?php echo e($record->fb_id); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->fb_id); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="fb_token"
							  data-value="<?php echo e($record->fb_token); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->fb_token); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="bio"
							  data-value="<?php echo e($record->bio); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->bio); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="nationality"
							  data-value="<?php echo e($record->nationality); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->nationality); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="phone_number"
							  data-value="<?php echo e($record->phone_number); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->phone_number); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="date"
							  data-name="bdate"
							  data-value="<?php echo e($record->bdate); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->bdate); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="rego"
							  data-value="<?php echo e($record->rego); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->rego); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="license"
							  data-value="<?php echo e($record->license); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->license); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="textarea"
							  data-name="paypal_email"
							  data-value="<?php echo e($record->paypal_email); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->paypal_email); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="gmail"
							  data-value="<?php echo e($record->gmail); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->gmail); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="text"
							  data-name="image_url"
							  data-value="<?php echo e($record->image_url); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->image_url); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="number"
							  data-name="avg_reviews"
							  data-value="<?php echo e($record->avg_reviews); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->avg_reviews); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="textarea"
							  data-name="password"
							  data-value="<?php echo e($record->password); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->password); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="number"
							  data-name="device_type"
							  data-value="<?php echo e($record->device_type); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->device_type); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="textarea"
							  data-name="push_token"
							  data-value="<?php echo e($record->push_token); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->push_token); ?></span>
						</td>
					<td>
						<span class="editable"
							  data-type="textarea"
							  data-name="remember_token"
							  data-value="<?php echo e($record->remember_token); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->remember_token); ?></span>
						</td>
					<td>
						<?php echo e($record->created_at); ?>

						</td>
					<td>
						<?php echo e($record->updated_at); ?>

						</td>
					<td>
						<span class="editable"
							  data-type="number"
							  data-name="active"
							  data-value="<?php echo e($record->active); ?>"
							  data-pk="<?php echo e($record->{$record->getKeyName()}); ?>"
							  data-url="/user/<?php echo e($record->{$record->getKeyName()}); ?>"
							  ><?php echo e($record->active); ?></span>
						</td>
					<?php echo $__env->make( 'vendor.crud.single-page-templates.common.actions', [ 'url' => 'user', 'record' => $record ] , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		    	</tr>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); if ($__empty_1): ?>
				<?php echo $__env->make('vendor.crud.single-page-templates.common.not-found-tr',['colspan' => 24], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	    	<?php endif; ?>
	    </tbody>

	</table>

	<?php echo $__env->make('vendor.crud.single-page-templates.common.pagination', [ 'records' => $records ] , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script>
	$(".editable").editable({ajaxOptions:{method:'PUT'}});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('vendor.crud.single-page-templates.common.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>