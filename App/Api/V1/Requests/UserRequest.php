<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.get_profile.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
