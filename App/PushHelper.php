<?php

namespace App;

use Config;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
class PushHelper
{
public function sendPush( $token,$message,$data){


    $this->sendAndroid($token, $message,$data);



}
 function sendAndroid($token, $message,$data){
  //  $message = 'test';
   $optionBuiler = new OptionsBuilder();
   $optionBuiler->setTimeToLive(60*20);

   $notificationBuilder = new PayloadNotificationBuilder('Buckleup');
   $notificationBuilder->setBody($message)
              ->setSound('default');

   $dataBuilder = new PayloadDataBuilder();
   $data['message'] = $message;
   $dataBuilder->addData($data);

   $option = $optionBuiler->build();
   $notification = $notificationBuilder->build();
   $data = $dataBuilder->build();

  //  $token = "c3FYfmAK-WA:APA91bGUkZbjz_yojlfSjy7SniycdgHT-MHX2JDds30hT0Imr4DhkGV15hoG-G8I38BmGUKUL8fiTCw8jLzuDQvRXm84-PfptcAtjsRDz2TmFT44t784lhnexmnxB9RtkAz6hyibSnBL";

   $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

   $downstreamResponse->numberSuccess();
   $downstreamResponse->numberFailure();
   $downstreamResponse->numberModification();

   //return Array - you must remove all this tokens in your database
   $downstreamResponse->tokensToDelete();

   //return Array (key : oldToken, value : new token - you must change the token in your database )
   $downstreamResponse->tokensToModify();

   //return Array - you should try to resend the message to the tokens in the array
   $downstreamResponse->tokensToRetry();
   return response()->json([
       'status' => 'success',
       'code' => 200

   ], 200);
}
}
