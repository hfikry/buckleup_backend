<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class FutureReview extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $table = "future_reviews";

    protected $fillable = [
        'user_writer', 'user_receiver', 'trip_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];


}
